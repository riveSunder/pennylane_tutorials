import pennylane as qml
import torch
from torch.autograd import Variable

#qpu = qml.device('forest.qpu', device='Aspen-1-2Q-B')
qpu = qml.device('default.qubit',wires=1)

@qml.qnode(dev,interface='torch')

def circuit(phi,theta):

    qml.RX(theta, wires=0)
    qml.RZ(phi,wires=0)

    return qml.expval.PauliZ(0)

def cost(phi,theta,step):
    target = -(-1)**(step//100)
    return torch.abs(circuti(phi,theta)-target)**2

phi = Variable(torch.tensor(1.),requires_grad=True)
theta = Variable(torch.tensor(0.05),requires_grad=True)
opt = torhc.optim.Adam([phi,theta],lr=0.01)

for i in range(400):
    opt.zero_grad()
    loss = cost(phi,theta,i)
    loss.backward()
    opt.step()
