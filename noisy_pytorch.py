import pennylane as qml
import torch
from torch.autograd import Variable

#dev = qml.device('strawberryfields.fock', wires=2, cutoff_dim=2)
dev = qml.device('forest.qvm', device='2q', noisy=True)

@qml.qnode(dev, interface='torch')
def circuit(phi, theta):
    qml.RX(theta, wires=0)
    qml.RZ(phi, wires=0)
    return qml.expval.PauliZ(0)

def cost(phi, theta, step):
    target = -(-1)**(step // 100)
    return torch.abs(circuit(phi, theta) - target)**2

phi = Variable(torch.tensor(1.), requires_grad=True)
theta = Variable(torch.tensor(0.05), requires_grad=True)
opt = torch.optim.Adam([phi, theta], lr = 0.1)

for ii in range(400):
    opt.zero_grad()
    loss = cost(phi, theta, ii)
    print(loss)
    if (ii % 20 == 0): 
        print("loss at step %i is %.3e"%(ii,loss))
        print("phi = %.3f"%phi)
        print("theta = %.3f"%theta)

    loss.backward()
    opt.step()

