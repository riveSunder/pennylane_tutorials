import pennylane as qml
from pennylane import numpy as np

# hyperparameters
step_size = 4e-1
step_size_qupr = 4e-1
steps = 100


# create devices
dev_fock = qml.device('strawberryfields.fock', wires=2, cutoff_dim=2)
dev_qubit = qml.device("default.qubit", wires=1)
dev_fock10 = qml.device("strawberryfields.fock", wires=2, cutoff_dim=10)


@qml.qnode(dev_fock)
def photon_redirection(params):
	qml.FockState(1,wires=0)
	qml.Beamsplitter(params[0], params[1], wires = [0, 1])
	return qml.expval.MeanPhoton(1)

@qml.qnode(dev_qubit)
def qubit_rotation(phi1, phi2):
	qml.RX(phi1, wires=0)
	qml.RY(phi2, wires=0)
	return qml.expval.PauliZ(0)

@qml.qnode(dev_fock10)
def photon_redirection10(params):
	"""photon redirection QNode"""

	qml.FockState(1, wires=0)
	qml.Beamsplitter(params[0], params[1], wires=[0,1])
	return qml.expval.MeanPhoton(1)

def squared_difference(x,y):
	"""node computes squared difference between inputs (classical)"""
	return np.abs(x-y)**2

def cost(params):
	return -photon_redirection(params)

def cost_qupr(params, phi1=0.5, phi2=0.1):
	"""returns squared difference between photon-redirection 
	and qubit rotation QNodes"""
	qubit_result = qubit_rotation(phi1, phi2)
	photon_result = photon_redirection10(params)

	return squared_difference(qubit_result, photon_result)

if __name__ == "__main__":

	init_params = np.array([0.01,0.009])

	opt = qml.GradientDescentOptimizer(stepsize= step_size)

	params = init_params

	print("optimizing quantum circuit . . .")
	for ii in range(steps):
		# update params (attempt to maximize mean photon count on 2nd wire)
		params = opt.step(cost, params)
		if ((ii+1) % 5 == 0):
			print(" Cost after step {:5d}: {: .7f}".format(ii, cost(params)))

	print(" Optimized rotation angles : ", params )


	opt_qupr = qml.GradientDescentOptimizer(stepsize=step_size_qupr)


	params = np.array([0.01,0.01])

	for ii in range(50):

		params = opt_qupr.step(cost_qupr,params)

		if ((ii+1) % 5 == 0):
			print(" Cost after step %i: %.7f"%(ii,cost_qupr(params)))

	print(" Optimized rotation angles (match photon-redirection to fixed qubit rotation) : {}".format(params))

	print("photon redirection result: {} \n and qubit rotation result: {} \n should now be the same for photon redirection parameters theta and phi = {}".format(photon_redirection10(params),qubit_rotation(0.5,0.1), params))
